import React from 'react';
import {Link} from 'react-router';

const HomePage = () => {
  return (
    <div>
      <h1>Mulapa</h1>
      <h2>Votre comptabilité enfin simplifiée !</h2>
    </div>
  );
};

export default HomePage;
