# Mulapa

Outil pour gérer soi-même la comptabilité de son entreprise et satisfaire sereinement aux obligations associées.

## Remarques

Cet outil repose sur [react-slingshot](https://github.com/coryhouse/react-slingshot) créé par [Cory House](https://github.com/coryhouse).
